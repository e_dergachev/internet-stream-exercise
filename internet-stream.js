const http = require('http');

let N = 444;
let fileSize = 0;

http.get(`http://kodaktor.ru/api2/buffer2/${N}`, stream => {
	stream.on('data', data => {
		if ((data[0] >= 65 && data[0] <= 90) || (data[0] >= 97 && data[0] <= 122)) { //decimals for latin letters
            console.log(`${fileSize} bytes before ${data}.`);
        }
		fileSize += data.length;
	});
	stream.on('end', () => {
		console.log(
            `V1 = ${fileSize} bytes
            V1/${N} = ${fileSize/N}`);
	});
});
